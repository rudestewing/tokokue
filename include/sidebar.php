
<div class="sidebar">

  <?php
  @session_start();
  if(!isset($_SESSION['login_user'])){
   header ('location: http://' . $_SERVER['HTTP_HOST'] . '/tokokue/admin/index.php');
  }
  
  ?>

  <ul class="navigation">

    <li><a href="/tokokue/admin/index.php" > <h3> Home </h3> </a></li>

    <li><span class="spacer">Data Master</span></li>

    <li><a href="/tokokue/admin/admin/admin.php">Admin</a></li>
    <li><a href="/tokokue/admin/customer/customer.php">Customer</a></li>
    <li><a href="/tokokue/admin/produk/produk.php">Produk</a></li>
    <li><a href="/tokokue/admin/biaya/biaya.php">Biaya Kirim</a></li>
    <li><span class="spacer">Transaksi</span></li>
    <li><a href="/tokokue/admin/konfirmasi/konfirmasi.php">Konfirmasi Transaksi </a></li>
    <li><a href="/tokokue/admin/transaksi/transaksi.php"> Daftar Transaksi </a></li>
    <li><a href="/tokokue/admin/pengiriman/pengiriman.php"> Pengiriman </a> </li>
    <li><span class="spacer">Laporan</span></li>
    <li><a href="/tokokue/admin/laporan/laporan_penjualan.php"> Penjualan</a> </li>
    


  <?php
    if($_SESSION['login_user']){
    ?>
      <li>
      <a href="<?php echo 'http://' .$_SERVER['HTTP_HOST']. '/tokokue/include/logout.php'; ?>" class="btn btn-danger"> LOGOUT </a>
      </li>
    <?php

    echo '<li style="color:#d1d1d1; font-style:italic;"> nama admin : ' .$_SESSION['login_user'].'<br></li>';

    } ?>


  </ul>
</div>
z
