<?php include('../../include/koneksi.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      EDIT DATA KOTA
    </title>

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">

  </head>
  <body>
    <?php include('../../include/sidebar.php'); ?>

    <?php

    $id_kota = $_GET['id'];
    $show = mysqli_query($con,"SELECT * FROM kota WHERE id_kota='$id_kota'");

    if(mysqli_num_rows($show) == 0 ){
      echo '<script> window.history.back() </script>';

    } else {
      $data = mysqli_fetch_array($show);
    }
    ?>
    <div class="main">
      <div class="page-header">
        <h1> EDIT DATA KOTA </h1>
      </div>

      <form class="form-horizontal pull left" action="update.php" method="post">

        <div class="form-group">
          <label class="col-sm-2 control-label"> Id Kota </label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="id_kota" value="<?php echo $data['id_kota']; ?>" >
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Nama Kota </label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="nama_kota" value="<?php echo $data['nama_kota']; ?>" >
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Biaya </label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="biaya" value="<?php echo $data['biaya']; ?>" >
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> </label>
          <div class="col-sm-3">
            <button type="submit" name="update" class="btn btn-primary"> UPDATE </button>
          </div>
        </div>

      </form>
    </div>
  </body>
</html>
