<?php include ('../../include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Data Kota Baru
    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">
  </head>
  <body>
    <?php include('../../include/sidebar.php'); ?>
    <div class="main">
      <div class="page-header">
        <h1> DATA KOTA BARU </h1>
      </div>
        <form class="form-horizontal pull left" action="add_process.php" method="post">

          <div class="form-group">
              <label class="col-sm-2 control-label"> Id Kota </label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="id_kota" placeholder="masukan id kota" style='text-transform:uppercase'  >
              </div>
          </div>

          <div class="form-group">
              <label class="col-sm-2 control-label"> Nama Kota </label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="nama_kota" placeholder="masukan nama kota">
              </div>
          </div>


          <div class="form-group">
              <label class="col-sm-2 control-label"> Biaya </label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="biaya" placeholder="masukan biaya kirim ">
              </div>
          </div>

          <div class="form-group">
              <label class="col-sm-2 control-label"> </label>
              <div class="col-sm-3">
                <button type="submit" name="add" class="btn btn-primary"> Input </button>
              </div>
          </div>
        </form>
    </div>


  </body>
</html>
