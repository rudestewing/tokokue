<?php include('../../include/koneksi.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Data
    </title>
    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">

  </head>

  <body>
    <?php include ('../../include/sidebar.php'); ?>


    <div class="main">
      <div class="page-header">
        <h1> DATA KOTA ( BIAYA KIRIM ) </h1>
      </div>

          <a href="add.php" class="btn pull-right btn-lg btn-primary"> + Data Biaya Baru </a>
        <table class="table table-striped">
          <thead>
              <tr>
                <th> no </th>
                <th> id kota </th>
                <th> nama kota </th>
                <th> biaya kirim </th>
                <th> action </th>
              </tr>
          </thead>

          <tbody>
            <?php
              $query = mysqli_query($con,"SELECT * FROM kota ORDER BY id_kota ASC ") or die(mysql_error());
              if (mysqli_num_rows($query) == 0) {

                 echo '<tr><td colspan="4"> Tidak ada data </td></tr>';

              } else {
                $no = 1;
                while ($data = mysqli_fetch_array($query)){
                  echo '<tr>';
                    echo '<td>' .$no. '</td>';
                    echo '<td>' .$data['id_kota']. '</td>';
                    echo '<td>' .$data['nama_kota']. '</td>';
                    echo '<td>' .$data['biaya']. '</td>';
                    echo '<td><a href="edit.php?id=' .$data['id_kota'].'" class="btn btn-primary btn-xs"> EDIT </a> <a href="delete.php?id=' .$data['id_kota'].'" onclick="return confirm(\'YAKIN?\')" class="btn btn-xs btn-danger"> DELETE </a></td>';
                  echo '</tr>';
                  $no++;
              }
            }
             ?>


          </tbody>

        </table>

    </div>
  </body>
</html>
