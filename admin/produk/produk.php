<?php include ('../../include/koneksi.php'); ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>
      Menu Admin
    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">

  </head>

  <body>

    <?php include ('../../include/sidebar.php'); ?>
    <!-- #####################MAIN CONTENT##################### -->
    <div class="main">
      <div class="page-header">
        <h1>DATA PRODUK</h1>
      </div>
      <a href="add.php" class="btn pull-right btn-lg btn-primary"> + Data Barang Baru </a><br><br>
      <table class="table table-striped">
        <thead>
          <tr>
            <th> no </th>

            <th> id produk </th>

            <th> nama produk </th>

            <th> harga </th>

            <th> stok </th>

            <th> deskripsi </th>

            <th> img </th>

            <th style="text-align:center"> action </th>
          </tr>
        </thead>
        <tbody>

          <?php
            $query = mysqli_query($con,"SELECT * FROM produk ORDER BY id_produk ASC") or die(mysql_error());

            if(mysqli_num_rows($query) == 0){
              echo '<tr><td colspan="4"> Tidak ada data </td></tr>';
            } else {
              $no = 1;
              while ($data = mysqli_fetch_array($query)){
                echo '<tr>';
                  echo '<td>' .$no. '</td>';

                  echo '<td>' .$data['id_produk']. '</td>';

                  echo '<td>' .$data['nama_produk']. '</td>';

                  echo '<td>' .$data['harga_produk']. '</td>';

                  echo '<td>' .$data['stok']. '</td>';

                  echo '<td class="col-sm-2" style="">' .$data['keterangan']. '</td>';

//                  echo '<td class="col-sm-2"> <img src="produk_img/"'.$data['gambar']. class> '</td>';
                  echo "<td> <img width='50' height='50' src='produk_img/" .$data['gambar']. "'> </td>";

                  echo '<td style="text-align:center"><a href="edit.php?id=' .$data['id_produk'].'" class="btn btn-primary btn-xs"> EDIT DATA </a> | <a href="delete.php?id=' .$data['id_produk'].'" onclick="return confirm(\'YAKIN?\')" class="btn btn-xs btn-danger"> DELETE </a> | <a href="editgambar.php?id='.$data['id_produk'].'" class="btn btn-primary btn-xs"> UPDATE GAMBAR  </td> ';

                echo '</tr>';

                $no++;
              }
            }
          ?>

        </tbody>
      </table>
    </div>
  </body>
</html>
