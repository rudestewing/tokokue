<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      EDIT DATA PRODUK
    </title>
    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
  </head>
  <body>
    <?php
    include('../../include/koneksi.php');

    $id_produk = $_GET['id'];
    $show=mysqli_query($con,"SELECT * FROM produk WHERE id_produk='$id_produk'");

    if(mysqli_num_rows($show) == 0 ){
      echo '<script>window.history.back()</script>';
    } else {
    $data = mysqli_fetch_array($show);
    }

     ?>
    <?php include('../../include/sidebar.php'); ?>



    <div class="main">
      <div class="page-header">
        <h1> EDIT DATA PRODUK </h1>
      </div>
      <form class="form-horizontal pull left" action="update.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label class="col-sm-2 control-label"> Id Produk </label>
          <div class="col-sm-3">
            <input type="text"  class="col-sm-2 form-control" name="id_produk" value="<?php echo $data['id_produk']; ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label"> Nama Produk </label>
          <div class="col-sm-3">
            <input type="text"  class="col-sm-2 form-control" name="nama_produk" value="<?php echo $data['nama_produk']; ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label"> Harga Produk </label>
          <div class="col-sm-3">
            <input type="text"  class="col-sm-2 form-control" name="harga_produk" value="<?php echo $data['harga_produk']; ?>">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label"> stok </label>
          <div class="col-sm-3">
            <input type="text"  class="col-sm-2 form-control" name="stok" value="<?php echo $data['stok']; ?>">
          </div>
        </div>


        <div class="form-group">
          <label class="col-sm-2 control-label"> Keterangan </label>
          <div class="col-sm-3">
            <textarea type="text" class="form-control" name="keterangan" rows="3"> <?php echo $data['keterangan']; ?> </textarea>

          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label"></label>
          <div class="col-sm-3">
            <button type="submit" class="btn btn-primary" name="update" > UPDATE </button>
          </div>
        </div>
      </form>


    </div>
  </body>
</html>
