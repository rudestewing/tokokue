
<!DOCTYPE html>
<?php include ('../../include/koneksi.php'); ?>


<html>
  <head>
    <meta charset="utf-8">
    <title>
      Tambah Data Produk
    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">

  </head>
  <body>
  <?php include ('../../include/sidebar.php'); ?>

<div class="main">
  <div class="page-header">
    <h1>Input Data Produk</h1>
  </div>
  <form class="form-horizontal pull left" action="add_process.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label class="col-sm-2 control-label"> ID Produk </label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="id_produk" placeholder="ID Produk">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label"> Nama Produk </label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="nama_produk" placeholder="Masukan Nama Produk">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label"> Harga </label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="harga_produk" placeholder="Masukan Harga">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label"> Jumlah </label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="stok" placeholder="Masukan Jumlah Barang">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label"> Keterangan </label>
        <div class="col-sm-5">
          <textarea class="form-control" name="keterangan" rows="3" placeholder="Isi Keterangan Disini"></textarea>
        </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label"> </label>


      <div class="col-sm-1">
        <input type="file" name="gambar">
      </div>


    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="add" class="btn btn-primary"> Input </button>
      </div>
    </div>

</div>

</form>
  </body>
</html>
