<?php  include('../../include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Rincian Penjualan
    </title>
    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">

    <style type="text/css">

      .detail{
        margin-left:10%;
        margin-right:10%;
        padding:50px;
        background-color:#e8e8e8;
        border-style: solid;
        border-width: 2px;
      }

    </style>
  </head>

  <?php

    $id_invoice=$_GET['id'];
    echo $id_invoice;

    $query_invoice=mysqli_query($con,"SELECT invoice.id_invoice,tagihan,total,
                                       pengiriman.id_pengiriman,
                                       customer.nama_customer,alamat,
                                       kota.nama_kota,biaya

                                       from invoice
                                       inner join pengiriman
                                       on invoice.id_invoice=pengiriman.id_invoice
                                       inner join customer
                                       on customer.id_customer=invoice.id_customer
                                       inner join kota
                                       on invoice.id_kota=kota.id_kota

                                       where invoice.id_invoice=$id_invoice

                                       ") or die(mysql_error());
  if($query_invoice){
    $data_invoice=mysqli_fetch_assoc($query_invoice);
  }

  $id_invoice=$data_invoice['id_invoice'];
  $id_pengiriman=$data_invoice['id_pengiriman'];
  $nama_customer=$data_invoice['nama_customer'];
  $nama_kota=$data_invoice['nama_kota'];
  $alamat=$data_invoice['alamat'];

  $biaya=$data_invoice['biaya'];
  $total=$data_invoice['total'];
  $tagihan=$data_invoice['tagihan'];
  ?>

  <body>
    <div class="detail">
      <div class="page-header" style="background-color:#dbdbdb">
          <h1>RINCIAN PENJUALAN</h1>
      </div>

      <div class="page-header" style="width:400px">
          <label class="label-control"> ID INVOICE : <?php echo $id_invoice; ?></label>
          <label class="label-control" style="float:right"> ID Pengiriman : <?php echo $id_pengiriman; ?> </label>
          <label class="label-control"> </label>
      </div>
      <div class="page-header">
          <label class="label-control col-sm-2"> Atas Nama </label>
          <label class="label-control"> : </label>
          <label class=label-control> <?php echo $nama_customer; ?></label>
      </div>

    <!--  <div class="page-header">
        <label class="label-control col-sm-2"> Kota Tujuan </label>
        <label class="label-control"> : </label>
        <label class=label-control> <?php // echo $nama_kota; ?></label>
      </div>
      <div class="page-header">
        <label class="label-control col-sm-2" style=""> Alamat Tujuan </label>
        <label class="label-control"> : </label>
        <label class=label-control> <?php // echo $alamat; ?></label>
      </div>
       -->

          <table class="table table-striped">
            <thead style="background-color:#dbdbdb">
              <tr>
                <th> No. </th>
                <th> ID Produk </th>
                <th> Nama Produk </th>
                <th> Harga </th>
                <th> Subtotal </th>
              </tr>
            </thead>

            <tbody style="background-color:white">

              <?php
              $query=mysqli_query($con,"SELECT invoice.id_invoice,
                                         transaksi.id_transaksi,
                                         detail.id_produk,subtotal,
                                         produk.nama_produk,harga_produk
                                         from invoice
                                         inner join transaksi
                                         on invoice.id_transaksi=transaksi.id_transaksi

                                         inner join detail
                                         on transaksi.id_transaksi=detail.id_transaksi

                                         inner join produk
                                         on detail.id_produk=produk.id_produk

                                         where invoice.id_invoice='$id_invoice'

                                         ") ;

              if($query){

                $no = 1;
                while($data=mysqli_fetch_array($query)){

                  echo "<tr>";
                     echo "<td>" .$no. "</td>";
                     echo "<td>" .$data['id_produk']. "</td>";
                     echo "<td>" .$data['nama_produk']. "</td>";
                     echo "<td>" .$data['harga_produk']. "</td>";
                     echo "<td>" .$data['subtotal']. "</td>";
                     $no++;

                  echo "</tr>";
                }
              }
              ?>
            </tbody>
              <thead>
                <tr>
                  <th colspan="4" style="" ></th>
                  <th colspan="1" style="" ></th>
                </tr>
              </thead>

              <thead>
                <tr>
                  <th colspan="4" style=""> TOTAL </th>
                  <th colspan="1" style=""> <?php echo $total; ?> </th>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th colspan="4" style=""> Biaya Kirim </th>
                  <th colspan="1" style=""> <?php echo $biaya; ?> </th>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th colspan="4" style=""> Tagihan </th>
                  <th colspan="1" style=""> <?php echo $tagihan; ?> </th>
                </tr>
              </thead>
          </table>

          <div class="button" style="text-align:center;">
            <a href="laporan_penjualan.php" class="btn btn-primary btn-lg"> Back </a>

          </div>
      </div>
  </body>
</html>
