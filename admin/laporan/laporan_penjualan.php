<?php include('../../include/koneksi.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Laporan Penjualan
    </title>
    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
  </head>


  <body>
    <?php
      $query=mysqli_query($con," SELECT invoice.tgl_invoice,total,tagihan,
                                  invoice.id_invoice,status,
                                  customer.nama_customer,
                                  kota.biaya
                                  from invoice
                                  inner join customer
                                  on invoice.id_customer=customer.id_customer
                                  inner join kota
                                  on invoice.id_kota=kota.id_kota

                                  where invoice.status='selesai'
                                  ORDER BY tgl_invoice ASC

                                  ") or die (mysql_error());

     ?>



    <?php  include('../../include/sidebar.php'); ?>
    <div class="main">
      <div class="page-header">
        <h1> LAPORAN PENJUALAN </h1>
        <div class="page-header">
          <h4> Di Filter Dari Seluruh Transaksi Yang Sudah Selesai
            <!-- <select class="" name="" style="height:30px; width:180px; font-style:italic;">
            <option value="" selected> default </option>
            <option value=""></option>
            <option value=""></option>
            <option value=""></option>  -->

                <?php
                $sum_query=mysqli_query($con,"SELECT sum(total) AS sum_total FROM invoice WHERE status='selesai' ") or die(mysql_error());
                  $data_sum = mysqli_fetch_assoc($sum_query);
                  $sum_total = $data_sum['sum_total'];
                ?>
          </select> <label style="float:right"> Total Penjualan : Rp. <?php echo number_format($sum_total); ?> </label> </h4>

              <h3 style="float:right"></h3>

        </div>
      </div>

      <table class="table table-striped">
        <thead>
          <tr>
            <th> Tanggal Invoice </th>
            <th> Atas Nama </th>
            <th> Total </th>
            <th> Biaya Kirim </th>
            <th> Tagihan </th>
            <th>  </th>
          </tr>
        </thead>

        <tbody>
              <?php
              if(mysqli_num_rows($query) == 0 ){
                echo "<tr> <td> tidak ada data! </td> </tr>";
              } else {
                while($data=mysqli_fetch_array($query)){

                      $tgl_invoice=$data['tgl_invoice'];
                      $nama_customer=$data['nama_customer'];
                      $total=$data['total'];
                      $biaya=$data['biaya'];
                      $tagihan=$data['tagihan'];

                echo '<tr>';
                echo '<td>' .$tgl_invoice. '</td>';
                echo '<td>' .$nama_customer. '</td>';
                echo '<td>' .$total. '</td>';
                echo '<td>' .$biaya. '</td>';
                echo '<td>' .$tagihan. '</td>';
                echo '<td> <a class="btn btn-primary btn-xs" href="rincian_penjualan.php?id=' .$data['id_invoice']. '"> RINCIAN  </a> </td>';
                echo '</tr>';
              }
              }
               ?>

        </tbody>

      </table>

    </div>

  </body>
</html>
