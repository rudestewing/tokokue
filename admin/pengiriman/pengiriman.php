  <?php include('../../include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      DATA PENGIRIMAN
    </title>
    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">


  </head>
  <body>
    <?php include('../../include/sidebar.php'); ?>

    <div class="main">
      <div class="page-header">
          <h1>DATA PENGIRIMAN</h1>
      </div>


      <table class="table table-striped">
        <thead>
          <tr>
            <th> No </th>
            <th> ID Pengiriman / Resi  </th>
            <th> ID Invoice </th>
            <th> Atas Nama </th>
            <th> Kota Tujuan </th>
            <th> Alamat </th>
            <th> Status Pengiriman </th>
            <th> action </th>
          </tr>

          <tbody>
            <?php
            $query=mysqli_query($con,"SELECT pengiriman.id_pengiriman,status_pengiriman,
                                       invoice.id_invoice,
                                       customer.nama_customer,alamat,
                                       kota.nama_kota
                                       from pengiriman
                                       inner join invoice
                                       on invoice.id_invoice=pengiriman.id_invoice
                                       inner join customer
                                       on customer.id_customer=invoice.id_customer
                                       inner join kota
                                       on kota.id_kota=invoice.id_kota
                                       ") ;


            if(mysqli_num_rows($query) ==0 ){
              echo '<tr> <td> Tidak Ada Data Pengirmian </td> </tr>';

            } else {
              $no=1;
              while($data=mysqli_fetch_array($query)){
                echo '<tr>';
                  echo '<td>' .$no. '</td>';
                  echo '<td>' .$data['id_pengiriman']. '</td>';
                  echo '<td>' .$data['id_invoice']. '</td>';
                  echo '<td>' .$data['nama_customer']. '</td>';
                  echo '<td>' .$data['nama_kota']. '</td>';
                  echo '<td>' .$data['alamat']. '</td>';
                  echo '<td>' .$data['status_pengiriman']. '</td>';?>
                  <td> <a href="selesai.php?id=<?php echo $data['id_pengiriman']; ?>"  onclick="return confirm('anda yakin?');"> Transaksi Selesai </a></td>


                <?php echo '</tr>';
                $no++;

              }
            }
             ?>

          </tbody>
        </thead>
      </table>


      </form>


    </div>


  </body>
</html>
