<?php
@session_start();
include('../../include/koneksi.php');
include('../../include/session.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Validasi
    </title>
    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">

  </head>

  <body>
    <?php $id_konfirmasi=$_GET['id']; ?>

    <?php $query=mysqli_query($con,"SELECT konfirmasi.bukti,
                                     konfirmasi.id_konfirmasi,
                                     invoice.tagihan,
                                     invoice.id_invoice,status,
                                     customer.nama_customer
                                     FROM konfirmasi
                                     INNER JOIN invoice
                                     ON konfirmasi.id_invoice=invoice.id_invoice
                                     INNER JOIN customer
                                     ON invoice.id_customer=customer.id_customer
                                     WHERE konfirmasi.id_konfirmasi='$id_konfirmasi'") or die(mysql_error());

    if($query){
      $data = mysqli_fetch_array($query);
      $status_pembayaran = $data['status'];
    }

    ?>



<form class="form-horizontal" action="validasi_process.php" method="post">

  <div class="">
    <?php include('../../include/sidebar.php'); ?>
  </div>
    <div class="main">
      <div class="page-header">
        <h1> VALIDASI PEMBAYARAN </h1>
      </div>
        <table class="table-striped" style="font-style:italic" style="background-color:#dbdbdb"> <!-- CSS INI BACKGROUND COLOR TIDAK KE LOAD. -->

          <thead>
            <tr>
              <th>
                <div class="page-header" style="margin-top:0">
                  <label class="" style="margin-right:70px"> ID INVOICE : <?php echo $data['id_invoice'] ?> </label>
                  <label class="" style="margin-right:70px" > Atas Nama : <?php echo $data['nama_customer'] ?> </label>
                  <label class="" style="margin-right:70px" > Tagihan : <?php echo $data['tagihan'] ?> </label>
                  <label class="" style="margin-right:70px" > Status Pembayaran : <?php echo $status_pembayaran ?> </label>
                  <label class="" style="margin-right:70px" >  </label>

                          <input type="hidden" name="id_konfirmasi" value="<?php echo $data['id_konfirmasi'] ?>">
                          <input type="hidden" name="id_invoice" value="<?php echo $data['id_invoice'] ?>">
                          <input type="hidden" name="id_admin" value="<?php echo $login_id_admin; ?>">

                </div>
              </th>
            </tr>
          </thead>
        </table>

        <div class="form-group">
          <div class="col-sm-3">

              <?php
              echo " <img width='400px' height='auto' src='konfirmasi_img/" .$data['bukti'].  "' style='border:2px' ' > ";
              ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-5">
            <?php
            if($status_pembayaran=='lunas'){
              echo '<input type="submit" class="btn btn-primary btn-lg" name="konfirmasi_lunas" value="KONFIRMASI LUNAS" disabled> | <a href="konfirmasi.php" class="btn btn-danger btn-lg"> Kembali </a>';

            } else {
              echo '<input type="submit" class="btn btn-primary btn-lg" name="konfirmasi_lunas" value="KONFIRMASI LUNAS"> | <a href="konfirmasi.php" class="btn btn-danger btn-lg"> Kembali </a>';

            }
            ?>


          </div>

        </div>

        <div class="col-sm-6">
        </div>
      </form>
    </div>

  </body>
</html>
