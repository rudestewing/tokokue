
<?php include('../../include/koneksi.php'); ?>
<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title>
      Konfirmasi Pembayaran
    </title>
  </head>
  <link rel="stylesheet" href="../../css/bootstrap.min.css">
  <link rel="stylesheet" href="../../css/master.css">

  <script language="javascript">
    function buka_popup(){
      window.open('validasi.php?id=<?php echo $data['id_konfirmasi']; ?>', '','width=640, height=480, menubar=yes,location=yes,scrollbars=yes, resizeable=yes, status=yes, copyhistory=no,toolbar=no');
    }
  </script>

  <body onLoad="">

    <?php include ('../../include/sidebar.php'); ?>

    <form class="" action="index.html" method="post">

      <div class="main">
        <div class="page-header">
          <h1> KONFIRMASI PEMBAYARAN </h1>
        </div>

      <table class="table table-striped">

          <thead>
            <tr>
              <th> No </th>
              <th> id_konfirmasi </th>
              <th> id invoice </th>
              <th> nama customer </th>
              <th> tagihan </th>
              <th>  bukti </th>
              <th> status </th>
              <th> action  </th>
            </tr>
          <tbody>
            <tr>
              <?php  $query = mysqli_query($con,"SELECT konfirmasi.id_konfirmasi,bukti,id_admin, invoice.tagihan,status, customer.nama_customer, konfirmasi.id_invoice
                                           from konfirmasi
                                           inner join invoice
                                           on konfirmasi.id_invoice = invoice.id_invoice
                                           inner join customer
                                           on invoice.id_customer = customer.id_customer
                                           WHERE invoice.status = 'bayar'
                                           ") ;

              if(mysqli_num_rows($query) == 0 ){
                  echo " <tr> <td> Data tidak ada! </td> </tr> ";

              } else {
                while ($data = mysqli_fetch_array($query)){
                  $no = 1;
                  echo "<tr>";
                    echo "<td>" .$no. "</td>";
                    echo "<td>" .$data['id_konfirmasi']. "</td>";
                    echo "<td>" .$data['id_invoice']. "</td>";
                    echo "<td>" .$data['nama_customer']. "</td>";
                    echo "<td>" .$data['tagihan']. "</td>";
                    echo "<td> <img width='50' height='50' src='konfirmasi_img/" .$data['bukti'].  "'> </td>";
                    echo "<td>" .$data['status']. "</td>";
                    // echo "<td> <a href='javascript: buka_popup();' class='btn btn-primary btn-xs'> VALIDASI </a> </td>";

                    echo "<td> <a href='validasi.php?id=" .$data['id_konfirmasi']. "' class='btn btn-primary btn-xs'> VALIDASI </a> </td>";


                  echo "</tr>";

                  $no++;


                }

              }

               ?>


            </tr>

          </tbody>
          </thead>
      </table>
      </div>
    </form>
  </body>
</html>
