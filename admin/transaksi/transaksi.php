<?php include('../../include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      List Transaksi
    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">
  </head>

  <body>

    <?php include('../../include/sidebar.php') ?>

    <div class="main">


        <div class="page-header">
          <h1> LIST TRANSAKSI </h1>
          <p> data dari invoice yang sudah masuk </p>

<!-- SORTIR DATA
        <form class="" action="" method="post">
              <select type="submit" class="pull-right col-sm-2" name="status">
                <option value="belum"> belum </option>
                <option value="bayar"> bayar </option>
                <option value="lunas"> lunas </option>
                <option value="lunas"> kirim </option>
                <option value="gagal"> gagal </option>
                <option value="lunas"> selesai </option>
              </select>
        </form>
 -->

        </div>




        <table class="table table-striped" style="">
          <thead>
            <tr>
              <th> No </th>
              <th> ID INVOICE </th>
              <th> Tanggal INVOICE </th>
              <th> Nama Customer </th>
              <th> Nama Kota </th>
              <th> Total </th>
              <th> Biaya Kirim </th>
              <th> Tagihan </th>
              <th> Status </th>
              <th> Action </th>
            </tr>
          </thead>

            <tbody>
              <?php
              $query = mysqli_query($con,"SELECT invoice.id_invoice,tgl_invoice,total,tagihan,status,
                                    transaksi.id_customer,
                                    customer.nama_customer,alamat,
                                    invoice.id_kota,
                                    kota.biaya,nama_kota
                                    FROM invoice
                                    INNER JOIN transaksi
                                    ON invoice.id_transaksi = transaksi.id_transaksi
                                    INNER JOIN customer ON customer.id_customer = transaksi.id_customer
                                    INNER JOIN kota ON kota.id_kota = customer.id_kota
                                    ORDER BY invoice.id_invoice DESC
                                    ")
                                    or die(mysql_error());

              if(mysqli_num_rows($query) == 0 ){
                echo "<tr> <td colspan='4'> Tidak Ada Data!  </td> </tr>";
              } else {
                $no = 1 ;
                while($data  = mysqli_fetch_array($query)){
                  $status=$data['status'];

                echo '<tr>';
                  echo '<td>' .$no. '</td>';
                  echo '<td>' .$data['id_invoice']. '</td>';
                  echo '<td>' .$data['tgl_invoice']. '</td>';
                  echo '<td>' .$data['nama_customer']. '</td>';
                  echo '<td>' .$data['nama_kota']. '</td>';
                  echo '<td>' .$data['total']. '</td>';
                  echo '<td>' .$data['biaya']. '</td>';
                  echo '<td>' .$data['tagihan']. '</td>';
                  echo '<td>' .$status. '</td>';

                  if($status == "lunas"){
                  echo '<td> <a style="" class="btn btn-primary btn-xs" href="detail_popup.php?id=' .$data['id_invoice']. '"> LIHAT DETAIL </a>';

                  } elseif($status =="kirim"){
                    echo '<td> <a style="background-color:green" class="btn btn-primary btn-xs" href="detail_popup.php?id=' .$data['id_invoice']. '"> LIHAT DETAIL </a>';

                  } elseif($status =="selesai"){
                    echo '<td> Cek Menu Laporan </td>';
                  } else {
                    echo'<td> <a style="background-color:red" class="btn btn-primary btn-xs" href="detail_popup.php?id=' .$data['id_invoice']. '"> LIHAT DETAIL </a>';

                  }
                  $no++;
                  echo '</tr>';
                }
              }
              ?>

            </tbody>
        </table>

    </div>

  </body>
</html>
