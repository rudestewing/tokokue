
<?php
@session_start();

include('../../include/koneksi.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      DETAIL TRANSAKSI
    </title>

    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">

    <?php
    $id_invoice=$_GET['id'];
    ?>
  </head>

  <body>

  <style type="text/css">
    .detail{
      margin-left: 40px;
      margin-right: 40px;
      padding: 50px;
    }
  </style>

<form class="form-control" action="kirim.php" method="post">

  <div class="detail">
    <div class="page-header">
      <?php $query_invoice=mysqli_query($con,"SELECT invoice.id_invoice,status, transaksi.id_transaksi, customer.nama_customer,alamat,
                                        kota.nama_kota
                                        from invoice
                                        inner join transaksi
                                        on invoice.id_transaksi=transaksi.id_transaksi
                                        inner join customer
                                        on transaksi.id_customer=customer.id_customer
                                        inner join kota
                                        on kota.id_kota=invoice.id_kota
                                        where invoice.id_invoice='$id_invoice'
                                        ");
      if($query_invoice){
          while ($data_invoice=mysqli_fetch_array($query_invoice)){
            $nama_customer=$data_invoice['nama_customer'];
            $nama_kota=$data_invoice['nama_kota'];
            $alamat=$data_invoice['alamat'];
            $status=$data_invoice['status'];
          }
      }
      ?>

      <h1> DETAIL TRANSAKSI </h1>
      <?php if($status == "lunas"){
                echo '<a  href="#" class="btn btn-primary" style="pointer-events:none"> SUDAH LUNAS  </a>';
      } else {
        echo '<a href="update-lunas.php?id='.$id_invoice.'" class="btn btn-danger" onclick="return confirm("are you sure?")"> UBAH DATA MENJADI LUNAS  </a>';
      }
      ?>

    </div>
    <table class="table-striped" style="font-style:italic" style="background-color:#dbdbdb"> <!-- CSS INI BACKGROUND COLOR TIDAK KE LOAD. -->

      <thead>
        <tr>
          <th>

            <div class="page-header" style="margin-top:0">
              <label class="" style="margin-right:70px"> ID INVOICE : <?php echo $id_invoice ?> </label>
              <label class="" style="margin-right:70px" > Atas Nama : <?php echo $nama_customer ?> </label> <br>
              <label class="" style="margin-right:70px" >  Alamat Tujuan : <?php echo "$alamat -  $nama_kota"; ?> </label><br>

              <label class="" style="margin-right:70px" > Status Pembayaran : <?php echo $status ?> </label>

                      <input type="text" name="id_invoice" value="<?php echo $id_invoice ?>">
            </div>
          </th>
        </tr>
      </thead>
    </table>
  <table class="table table stripped">


          <thead>
            <tr>
              <th> No </th>
              <th> ID Produk </th>
              <th class="col-sm-3"> Nama Produk </th>
              <th class="col-sm-4"> Jumlah </th>
              <th> Subtotal </th>
            </tr>
          </thead>
          <tbody>
            <?php
            $query=mysqli_query($con," SELECT invoice.id_invoice,total,tagihan,
                                        transaksi.id_transaksi,
                                        customer.id_customer,nama_customer,
                                        detail.id_produk,jumlah,subtotal,
                                        produk.nama_produk,harga_produk,
                                        kota.nama_kota,biaya
                                        from invoice
                                        inner join transaksi
                                        on invoice.id_transaksi=transaksi.id_transaksi
                                        inner join customer
                                        on transaksi.id_customer=customer.id_customer
                                        inner join detail
                                        on transaksi.id_transaksi=detail.id_transaksi
                                        inner join produk
                                        on detail.id_produk=produk.id_produk
                                        inner join kota
                                        on kota.id_kota=invoice.id_kota

                                        where invoice.id_invoice='$id_invoice'

                                         ") or die(mysql_error());

            if($query){

              while($data=mysqli_fetch_array($query)){
                $total=$data['total'];
                $biaya=$data['biaya'];
                $tagihan=$data['tagihan'];
                  echo '<tr>';
                    $no=1;
                    echo '<td>' .$no. '</td>';
                    echo '<td>' .$data['id_produk']. '</td>';
                    echo '<td>' .$data['nama_produk']. '</td>';
                    echo '<td>' .$data['jumlah']. '</td>';
                    echo '<td>' .$data['subtotal']. '</td>';
                    $no++;
                    echo '</tr>';
              }
            }
            ?>
          </tbody>

  </table>

    <div class="form-group">
      <div class="col-sm-6">
      </div>
      <label class="col-sm-4" style="background-color:#dbdbdb" > TOTAL </label>
      <label class="col-sm-2" style="background-color:#dbdbdb" > <?php echo $total ?></label>

    </div>

    <div class="form-group">
      <div class="col-sm-6">
      </div>
      <label class="col-sm-4" style="background-color:#dbdbdb" > BIAYA KIRIM </label>
      <label class="col-sm-2" style="background-color:#dbdbdb" > <?php echo $biaya ?></label>

    </div>

    <div class="form-group">
      <div class="col-sm-6">
      </div>
      <label class="col-sm-4" style="background-color:#dbdbdb" > TAGIHAN </label>
      <label class="col-sm-2" style="background-color:#dbdbdb" > <?php echo $tagihan ?></label>

    </div>
    <div class="form-group">
      <div class="col-sm-6">
      </div>

      <?php

      if($status == "lunas"){
        echo '<input style="width:150px" type="submit" class="btn btn-primary btn-lg" name="kirim" value="Kirim Barang">';
      } else {
        echo '';
      }
      ?>

     <a style="width:150px" href="transaksi.php" class="btn btn-primary btn-lg" > Back </a>

    </div>
    </form>
  </body>

</html>
