
<?php include ('../../include/koneksi.php'); ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>
      Menu Admin
    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">

  </head>

  <body>
      <?php include ('../../include/sidebar.php'); ?>
    <div class="main">
      <div class="page-header">
        <h1>DATA ADMIN</h1>
      </div>


              <a href="add.php" class="btn pull-right btn-lg btn-primary"> + Data Admin Baru </a><br><br>
              <br> </br>

              <table class="table table-striped">
                <thead>
                  <tr>
                    <th> no </th>
                    <th> id admin </th>
                    <th> username </th>
                    <th> nama admin </th>
                    <th> action </th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    $query = mysqli_query($con,"SELECT * FROM admin ORDER BY id_admin ASC") or die(mysql_error());
                    if(mysqli_num_rows($query) == 0){
                      echo '<tr><td colspan="4"> Tidak ada data </td></tr>';
                    } else {
                      $no = 1;
                      while ($data = mysqli_fetch_array($query)){
                        echo '<tr>';
                          echo '<td>' .$no. '</td>';
                          echo '<td>' .$data['id_admin']. '</td>';
                          echo '<td>' .$data['username']. '</td>';
                          echo '<td>' .$data['nama_admin']. '</td>';
                          echo '<td><a href="edit.php?id=' .$data['id_admin'].'" class="btn btn-primary btn-xs"> EDIT </a> <a href="delete.php?id=' .$data['id_admin'].'" onclick="return confirm(\'YAKIN?\')" class="btn btn-xs btn-danger"> DELETE </a></td>';
                        echo '</tr>';
                        $no++;
                      }
                    }
                  ?>

                </tbody>
              </table>
            </div>

  </body>
</html>
