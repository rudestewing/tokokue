<?php include ('../../include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="../../css/master.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">

    <meta charset="utf-8">
    <title>
      Edit Data Admin
    </title>
  </head>

  <body>
    <?php include('../../include/sidebar.php'); ?>


    <?php
    $id_admin=$_GET['id'];
    $show=mysqli_query($con,"SELECT * FROM admin WHERE id_admin='$id_admin'");

    if(mysqli_num_rows($show) == 0 ){
      echo '<script>window.history.back()</script>';

    } else {
    $data = mysqli_fetch_array($show);
    }

    ?>

    <div class="main">
      <div class="page-header">
        <h1>Edit Data Admin</h1>
      </div>

      <form class="form-horizontal pull left" action="update.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label class="col-sm-2 control-label"> ID Admin </label>
          <div class="col-sm-3">
            <input type="text" class="col-sm-2 form-control" name="id_admin" value="<?php echo $data['id_admin'] ?>">
          </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"> nama admin </label>
            <div class="col-sm-3">
              <input type="text" class="col-sm-2 form-control" name="nama_admin" value="<?php echo $data['nama_admin']; ?>">
            </div>
        </div>



        <div class="form-group">
            <label class="col-sm-2 control-label"> username </label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="username" value="<?php echo $data['username']; ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"> Password </label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="password" value="<?php echo $data['password']; ?>" >
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">  </label>
            <div class="col-sm-3">
                <button type="submit" name="update" class="btn btn-primary"> UPDATE </button>
            </div>
        </div>



    </div>
  </div>

    </body>
</html>
