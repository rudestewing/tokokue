<?php include ('../../include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Tambah Data Admin

    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">

  </head>

  <body>
    <?php include ('../../include/sidebar.php'); ?>

<div class="main">
  <div class="page-header">
    <h1>Input Data Admin</h1>
  </div>

  <form class="form-horizontal pull left" action="add_process.php" method="post">
    <div class="form-group">
        <label class="col-sm-2 control-label"> Id Admin </label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="id_admin" placeholder="masukan id admin">
        </div>
    </div>
    <div class="form-group"> 
        <label class="col-sm-2 control-label"> Nama </label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="nama_admin" placeholder="masukan nama">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"> username </label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="username" placeholder="masukan username">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"> password </label>
        <div class="col-sm-3">
          <input type="password" class="form-control" name="password" placeholder="masukan password">
        </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" name="add" class="btn btn-primary"> Input </button>
      </div>
    </div>

  </form>
</div>

  </body>
</html>
