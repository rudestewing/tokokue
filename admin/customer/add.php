<?php include("../../include/koneksi.php"); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Data Customer Baru
    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">

  </head>
  <body>
    <?php include('../../include/sidebar.php'); ?>

    <div class="main">
      <div class="page-header">
        <h1>Input Data Customer</h1>
      </div>

      <form class="form-horizontal pull left" action="add_process.php" method="post">

        <div class="row">
          <div class="form-group">
            <label class="col-sm-2 control-label"> ID Customer </label>          
            <div class="col-sm-3">
              <input type="text"  class="form-control" name="id_customer" placeholder="Input Nama Customer Disini">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group">
            <label class="col-sm-2 control-label"> Nama Customer </label>          
            <div class="col-sm-3">
              <input type="text"  class="form-control" name="nama_customer" placeholder="Input Nama Customer Disini">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="form-group">
            <label class="col-sm-2 control-label"> Username </label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="username" placeholder="Input Username">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Email </label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="email" placeholder="Email">
        </div>
        
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label"> Password </label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="password" placeholder="Input Password">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label "> Kota </label>
        <div class="col-sm-3">

            <select class="form-control" name="id_kota">
              <?php
              $query=mysqli_query($con,"SELECT * FROM kota");
              if(mysqli_num_rows($query)==0){
                echo '<option value=""> Tidak Ada Data </option>';

              } else {
                  while($data=mysqli_fetch_array($query)){
                  echo '<option  value='.$data['id_kota'].'>' .$data['nama_kota']. '</option>';
                  }
              }
              ?>
            </select>
        </div>
      </div>


      <div class="form-group">
          <label class="col-sm-2 control-label"> Alamat </label>
          <div class="col-sm-3">
            <textarea type="text" class="form-control" name="alamat" placeholder="Input Alamat"> </textarea>
        </div>
      </div>

      <div class="form-group">
          <label class="col-sm-2 control-label"> Nomor Telepon </label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="no_telepon" placeholder="Input Nomor Telepon">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="add" class="btn btn-primary"> Input </button>
        </div>
      </div>
  </form>


    </div>
  </body>
</html>
