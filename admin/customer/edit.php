<?php include('../../../tokokue/include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      EDIT DATA CUSTOMER
    </title>
  </head>
  <link rel="stylesheet" href="../../css/bootstrap.min.css">
  <link rel="stylesheet" href="../../css/master.css">

  <body>
    <?php  include ('../../include/sidebar.php'); ?>

    <?php
    $id_customer=$_GET['id'];
    $show=mysqli_query($con,"SELECT * FROM customer WHERE id_customer='$id_customer'");

    if(mysqli_num_rows($show) == 0){
        echo '<script>window.history.back()</script>';
    } else {
      $data=mysqli_fetch_array($show);
    }
    ?>

    <div class="main">
      <div class="page-header">
        <h1> Edit Data Customer </h1>
      </div>

      <form class="form-horizontal pull left" action="update.php" method="post">


        <div class="form-group">
          <label class="col-sm-2 control-label">  </label>
          <div class="col-sm-3">
            <input type="hidden" class="form-control"  name="id_customer" value="<?php echo $data['id_customer']; ?>">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Username </label>
          <div class="col-sm-3">
            <input type="text" class="col-sm-2 form-control" name="username" value="<?php echo $data['username']; ?>">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Password </label>
          <div class="col-sm-3">
            <input type="text" class="col-sm-2 form-control" name="password" value="<?php echo $data['password']; ?>">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Nama Customer </label>
          <div class="col-sm-3">
            <input type="text" class="col-sm-2 form-control" name="nama_customer" value="<?php echo $data['nama_customer']; ?>">
          </div>
        </div>


        <div class="form-group">
          <label class="col-sm-2 control-label"> Kota </label>
          <div class="col-sm-3">

            <?php
            $id_kota=$data['id_kota'];

            $tampil= mysqli_fetch_array(mysqli_query($con,"SELECT nama_kota FROM kota WHERE id_kota='$id_kota'"));
            ?>


            <select class="form-control" name="id_kota" >

              <?php
              $query=mysqli_query($con,"SELECT * FROM kota");
              if(mysqli_num_rows($query)==0){
                echo '<option value=""> Tidak Ada Data </option>';

              } else {
                  echo "<option value='" .$tampil['id_kota']. "' selected>" .$tampil['nama_kota']. "</option>";

                  while($data_kota=mysqli_fetch_array($query)){

                  echo "<option value='" .$data_kota['id_kota']. "'>" .$data_kota['nama_kota']. "</option>";

                  }
              }
              ?>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Alamat </label>
          <div class="col-sm-3">
            <textarea type="text" class="form-control" name="alamat"><?php echo $data['alamat']; ?></textarea>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"> Nomor Telepon </label>
          <div class="col-sm-3">
            <input type="text" class="col-sm-2 form-control" name="no_telepon" value="<?php echo $data['no_telepon']; ?>">
          </div>
        </div>

        <div class="form-group">
        <label class="col-sm-2 control-label"></label>
          <div class="col-sm-3">
            <button type="submit"  class="btn btn-primary" name="update"> UPDATE </button>
          </div>
        </div>

      </form>
    </div>
  </body>
</html>
