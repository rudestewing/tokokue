<?php include('../../include/koneksi.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>
      Menu Customer
    </title>
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/master.css">
  </head>
  <body>

  <?php include ('../../include/sidebar.php'); ?>

<div class="main">


  <div class="page-header">
    <h1> DATA CUSTOMER </h1>

    <a style="margin-bottom:5px" href="add.php" class="btn pull-right btn-lg btn-primary"> + Data Customer Baru </a><br>

    <table class="table table-striped">
      <thead style="background-color:#dbdbdb">
        <tr>
          <th> no </th>
          <!-- <th> id customer </th> -->
          <th> nama customer </th>
          <th> username  </th>
          <th> kota </th>
          <th width:'100px'> alamat </th>
          <th> no telpon </th>
          <th> action </th>
        </tr>
      </thead>
  </div>

  <tbody style="background-color:white">
      <?php
    $query = mysqli_query($con,"SELECT * FROM customer ORDER BY id_customer ASC") or die(mysql_error());
    if(mysqli_num_rows($query)==0){
      echo '<tr><td colspan="4"> Tidak ada data </td></tr>';
    } else {
      $no = 1;
      while($data=mysqli_fetch_array($query)){
        echo '<tr>';
          echo '<td>' .$no. '</td>';
          // echo '<td>' .$data['id_customer']. '</td>';
          echo '<td>' .$data['nama_customer']. '</td>';
          echo '<td>' .$data['username']. '</td>';

          $id_kota=$data['id_kota'];

          $tampil=mysqli_fetch_array(mysqli_query($con,"SELECT nama_kota FROM kota WHERE id_kota='$id_kota'"));

          echo '<td>' .$tampil['nama_kota']. '</td>';
          echo '<td width="150px">' .$data['alamat']. '</td>';
          echo '<td>' .$data['no_telepon']. '</td>';
          echo '<td><a href="edit.php?id=' .$data['id_customer'].'" class="btn btn-primary btn-xs"> EDIT </a> <a href="delete.php?id=' .$data['id_customer'].'" onclick="return confirm(\'YAKIN?\')" class="btn btn-xs btn-danger"> DELETE </a></td>';
          echo '</tr>';
      $no++;
      }
    }
    ?>
    </tbody>
  </table>
</div>
  </body>
</html>
